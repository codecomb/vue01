//main.js程序入口文件，是初始化vue实例并使用需要的插件，加载各种公共组件

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'            //import Vue from "../node_modules/vue/dist/vue.js";
import App from './App'          //完整写法：import App form './App.vue'
import router from './router'
import axios from 'axios'


Vue.config.productionTip = false
axios.defaults.withCredentials=true;  //withCredentials默认是false，意思就是不携带cookie信息，那就让它为true，我是全局性配置的，就是main.js中的这句话

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
